# README #

1/3スケールのシャープ ジョイカードCZ-8NJ1風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK FUSION360です。


***

# 実機情報

## メーカ
- シャープ

## 発売時期

- -

## 参考資料

- ｰ

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-8nj1/raw/e5a5da57909cbe46e5092b9656e1d60557ab310c/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-8nj1/raw/e5a5da57909cbe46e5092b9656e1d60557ab310c/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-8nj1/raw/e5a5da57909cbe46e5092b9656e1d60557ab310c/ExampleImage.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-8nj1/raw/e5a5da57909cbe46e5092b9656e1d60557ab310c/ExampleImage_2.png)

